set sysinfo to system info
set osver to system version of sysinfo
global desktopPictureFolder

-- Set OS-specific options
considering numeric strings
  if osver >= 10.15 then
    log "We're running Catalina"
    set desktopPictureFolder to "/System/Library/Desktop Pictures"
  else if osver >= 10.14 then
    log "We're running Mojave"
    set desktopPictureFolder to "/Library/Desktop Pictures"
  else
    log "We're running something unsupported... os version is " & osver & "!"
    error number -128
  end if
end considering

-- Prompt the user to pick an image
set theImage to choose file with prompt "Please select an image:" ¬
  of type {"public.image"} ¬
  default location desktopPictureFolder

-- Set the selected image as the Desktop picture for every detected monitor
tell application "System Events"
  tell every desktop
    log "Setting Desktop Background to " & POSIX path of theImage
    set picture to POSIX path of theImage
    # Unfortunately the next line simply won't compile pre-Cataline
    # so, unless someone can tell me different, it can't even be an OS-dependent option
    -- set dynamic style to dynamic
    end tell
end tell

