# Applescript Utils

A collection of applescript utilities you might find useful.

##  Development

My preference is Visual Studio Code with the AppleScript plugin.

Make sure the file extension for scripts is `.applescript`

To type the line-continuation character, it's Option + l (lowercase L)


## Running the scripts

Save as an applcation and add to ~/Applications.  You can then run from Spotlight (Cmd + Space).  When you run in this mode, you 
may get a security prompt (e.g. Application is trying to access System Events) - just click ok to proceed.

Alternatively, save as a Scruipt and then run with osascript at the command-line, e.g.

```
osascript ./set-every-desktop.applescript
```

